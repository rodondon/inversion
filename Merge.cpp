#include <iostream>
#include <fstream>
#include "MergeS.h"

using namespace std;
 Merge::Merge(int size)
{
	dim = size;
	inversion = 0;
	merged=new int[size];
	for (int i = 0; i < size; i++)
		merged[i] = 0;
}
void Merge::mergeSort(int list[], int size)
{
	sort(list, 0,size-1);
}

void Merge::sort(int list[],int min, int max)
{
	if (max - min <1)
		return;

	int middle = (max + min) / 2;
	
	sort(list, middle + 1, max);
	sort(list, min, middle);
	int leftEnd = middle + 1;
	merge(list, min, middle, leftEnd, max);

}
Merge::~Merge()
{
	delete[] merged;
}
void Merge::merge(int list[],int& leftBegin, int& leftEnd, int& rightBegin, int& rightEnd)
{
	int a = leftBegin;
	int b = rightBegin;
	for (int k = 0; k < dim; k++)
	{
		if (a <= leftEnd && b <= rightEnd)
		{
			if (list[a] < list[b])
			{
				merged[k] = list[a];
				a++;
			}
			else if (list[b] < list[a])
			{
				merged[k] = list[b];
				b++;
				inversion+=leftEnd+1-a;
			}
		}
		else if (a>leftEnd && b <= rightEnd)
		{
			merged[k] = list[b];
			b++;
		}
		else if (b > rightEnd && a <= leftEnd)
		{
			merged[k] = list[a];
			a++;
		}
	}
	b = 0;
	//put the merged sub-array back in list[] for use be other recursive calls.
	for (int t = leftBegin; t <= rightEnd; t++)
	{
			list[t] = merged[b];
			b++;
	}

}
int Merge::inversions()
{
	return inversion;
}
void Merge::print()
{
	for (int i = 0; i < dim; i++)
		printf("%d\n", merged[i]);
	printf("\nInversions : %d\n", inversion);
}
void Merge::printFile(string file){
	ofstream out(file);
	for (int i = 0; i < dim; i++)
		out << merged[i]<< endl;
	out << endl << "Inversions: " << inversion << endl;
}