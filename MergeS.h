#include <iostream>
#include <string>

using namespace std;
struct Merge{
	int * merged;
	unsigned long int inversion;
	int dim;
	Merge(int);
	void sort(int[], int min, int max);
	void merge(int[],int&,int&,int&,int&);
	void mergeSort(int[], int size);
	int inversions();
	void print();
	void printFile(string);
	~Merge();
};