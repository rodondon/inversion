#include <iostream>
#include "MergeS.h"
#include <fstream>
#include <ctime>
using namespace std;

int main()
{
	ifstream file("data.txt");
	Merge merge(250000);
	int i=0;
	int list[250000];
	while (file >> list[i] && i < 250000)
	{
		i++;
	}
	/*   Timer      */
	clock_t startClock, finishClock;
	double timeCount;
	startClock = clock();

	merge.mergeSort(list, 250000);
	
	/*  Finish Timer*/
	finishClock = clock();
	timeCount = finishClock - startClock;

	cout << "time: " << timeCount / 100 << endl;
	
	merge.printFile("out.txt");
	return 0;
}